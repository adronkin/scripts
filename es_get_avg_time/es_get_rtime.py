#!/usr/bin/env python
'''
Getting average response time from elasticsearch (can be use wildcard).
Start - python3 es_get_rtime_idms.py -c /home/user/conf_example.json -p /home/user/result/
'''

import json
import argparse
import elasticsearch_dsl


def get_args():
    '''Parse argument, get config file from args'''
    global DIR
    parser = argparse.ArgumentParser(description="Get speed of pages")
    parser.add_argument('-c', '--conf', action='store',
                        dest='conf_file', required=True)
    parser.add_argument('-p', '--path', dest='dir_path', required=True)
    args = parser.parse_args()
    parse_config(args.conf_file)
    DIR = args.dir_path


def parse_config(conf_file):
    '''load info from conf file'''
    global CONFIG
    try:
        with open(conf_file) as json_conf_file:
            CONFIG = json.load(json_conf_file)
    except IOerror as e:
        print("Unable to open file: {} {}".format(e.errno, e.strerror))


def prepering_request(mask):
    '''fill the request with conditions'''
    should = []
    should.append({mask: {'@fields.base_url.keyword': url}})
    send = elasticsearch_dsl.Search(using=ES, index=INDEX)
    send = send.filter({"range": {"@timestamp": {"gte": "now-15m", "lte": "now"}}})
    send = send.query(elasticsearch_dsl.Q('bool', should=should, minimum_should_match=1))
    send = send.extra(size=0)
    send.aggs.bucket('url', 'terms', field='@fields.base_url.keyword') \
        .metric('rtime', 'extended_stats', field="@fields.request_time")
    response = send.execute()
    return response.to_dict()


get_args()

ADDRESS_URL = CONFIG['urls']
EH = CONFIG['es']
INDEX = CONFIG['index']

try:
    ES = elasticsearch_dsl.connections.create_connection(hosts=EH)
except Exception as ex:
    print("Connection Error", ex)

for url in ADDRESS_URL:
    if '*' in url:
        response = prepering_request('wildcard')
        method = url.split('/')[1:-1]
        method = '_'.join(method)
    else:
        response = prepering_request('match')
        method = url.replace('/', '_')[1:]

    req_count = 0
    req_time_sum = 0

    for keys in response['aggregations']['url']['buckets']:
        for key in keys:
            if key == 'rtime':
                req_time_sum += keys[key]['sum']

        result_file = open('{}/{}.txt'.format(DIR, method), 'w', encoding='utf-8')
        avg_time = req_time_sum / req_count
        result_file.write('{:.2f}'.format(avg_time))
