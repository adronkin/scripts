'''
Send api request and save result to DB.
Start - python3 sync_discount.py -f config_example.cfg
'''

import sys
import time
import json
import getopt
import configparser
import requests
import cx_Oracle


try:
    OPTS, ARGS = getopt.getopt(sys.argv[1:], 'c:', ['config='])
except getopt.GetoptError:
    print('Error, no args!')
    sys.exit(1)

for opt, arg in OPTS:
    if opt in ('-c', '--config'):
        conffile = arg
    else:
        sys.exit(1)

CONFIG = configparser.ConfigParser()
CONFIG.read(conffile)
DBHOST = CONFIG.get('db', 'host')
DBPORT = CONFIG.get('db', 'port')
DBSID = CONFIG.get('db', 'sid')
DBUSER = CONFIG.get('db', 'user')
DBPASS = CONFIG.get('db', 'pass')

DSN = cx_Oracle.makedsn(DBHOST, DBPORT, DBSID)

try:
    BILL_CONNECT = cx_Oracle.connect(DBUSER, DBPASS, DSN, threaded=True)
    BILL_CURSOR = BILL_CONNECT.cursor()
except cx_Oracle.DatabaseError.info as info:
    print('Error connecting Oracle', info)
    sys.exit(1)

# Select row for request
SQL = 'select * from REQ91720_sync_discount where result is null and rownum = 1'
COUNT = 0
LINE = None

def send_request(line):
    '''ending request to api'''
    url = 'http://url.example.net/example'
    data = {'operation': 'addSOC', 'ctn': line[0], 'socId': line[1]}
    headers = {'Content-Type': 'application/json;charset=UTF-8'}
    request = requests.post(url, json=data, headers=headers)
    print('Request {} sended.'.format(line[0]))
    result = request.content
    result = json.loads(result.decode('utf-8'))
    if result['error']['code'] == 0:
        result = 'OK'
    else:
        result = result['error']['code']
    return result

def update_result(ctn, result):
    '''Insert result to DB'''
    sql = 'update inac.REQ91720_sync_discount set result = :result where mob_subs_key = :ctn'
    BILL_CURSOR.execute(sql, {'result': result, 'ctn': ctn[0]})
    BILL_CONNECT.commit()
    print('Update {} done. Result - {}.'.format(ctn, result))


while SQL:
    COUNT += 1
    BILL_CURSOR.execute(SQL)
    LINE = BILL_CURSOR.fetchall()
    try:
        update_result(LINE[0], send_request(LINE[0]))
        time.sleep(0.5)
    except IndexError:
        print('IndexError. Table processed.')
        break
#    if COUNT > 2000:
#        break

print('Script executed!')
